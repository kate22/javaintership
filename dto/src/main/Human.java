public class Human {
    String Name;
    String Surname;
    String Patronymic;
    int Age;
    String Gender;
    int Date;

    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Human)) return false;
        if (!super.equals(object)) return false;
        Human human = (Human) object;
        return getAge() == human.getAge() && getDate() == human.getDate() && java.util.Objects.equals(getName(), human.getName()) && java.util.Objects.equals(getSurname(), human.getSurname()) && java.util.Objects.equals(getPatronymic(), human.getPatronymic()) && java.util.Objects.equals(getGender(), human.getGender());
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), getName(), getSurname(), getPatronymic(), getAge(), getGender(), getDate());
    }

    public int getDate() {
        return Date;
    }

    public void setDate(int date) {
        Date = date;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getPatronymic() {
        return Patronymic;
    }

    public void setPatronymic(String patronymic) {
        Patronymic = patronymic;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Human{" +
                "Name='" + Name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Patronymic='" + Patronymic + '\'' +
                ", Age=" + Age +
                ", Gender='" + Gender + '\'' +
                ", Date=" + Date +
                '}';
    }
}